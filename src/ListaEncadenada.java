

import java.util.Iterator;


public class ListaEncadenada<T> implements ILista<T>{

	private Node<T> primero;
	private Node<T> actual;
	private int listSize;
	private Ordenar ordenar = new Ordenar();
	
	public ListaEncadenada()
	{
		primero = new Node<T>(null);
		actual = primero;
	}
	
	public int size()
	{
		return listSize;
	}
	
	@Override
	public Iterator<T> iterator() 
	{
		return new Iterator<T>() 
			{
			private Node<T> act=null;
	
			public boolean hasNext() 
			{
				if(act==null)return primero.getItem()!=null;
				else return act.getNext() != null;
			}
	
			public T next() 
			{
				if(act==null)
				{
					act=primero;
					if(act==null)return null;
					else return act.getItem();
				}
				else
				{
					if(act.getNext()!=null)
					{
						act = act.getNext();
						return act.getItem();
					}
					return null;
				}
	
			}
		};
}

	public void cambiar(int pos, T elem)
	{
		int i = 0;
		if(primero != null)
		{
			actual = primero;
			while(i<pos)
			{
				actual = actual.getNext();
				i++;
			}
			actual.setItem(elem);
		}
	}
	
	@Override
	public void agregarElementoFinal(T elem) 
	{
		if(primero.getItem() == null)
		{
			primero.setItem(elem);
			return;
		}
		else
		{
			Node<T> act = primero;
			while(act != null)
			{
				if (act.getNext()== null)
				{
					act.setNext(new Node<T>(elem));
					act.getNext().setItem(elem);
					actual = act.getNext();
					break;
				}
				act = act.getNext();
			}
		}
		listSize++;
	}
	
	public void agregarElementoPrincipio(T elem)
	{
		Node<T> newNode = new Node(elem);
		if(primero.getItem() == null)
		{
			primero.setItem(elem);
			return;
		}
		else
		{
			newNode.setNext(primero);
			primero=newNode;
		}
		listSize++;
	}
	
	public T quitarElementoPrincipio()
	{
		if(primero == null)
		{
			System.out.println("No hay elementos");
		}
		T elem = primero.getItem();
		Node<T> siguientePrimero = primero.getNext();
		primero.setItem(null);
		primero = siguientePrimero;
		listSize--;
		return elem;
	}

	@Override
	public T darElemento(int pos) 
	{
		actual = primero;
		int contador = 0;
		while(contador<pos)
		{
			actual = actual.getNext();
			contador++;
		}
		return (T) actual.getItem();
	}

	public T darMaximo()
	{
		Comparador comparador = new Comparador<>();
		ListaEncadenada a = this;
		ordenar.orden(a, comparador);
		return (T) a.darElemento(0);
	}
	
	@Override
	public int darNumeroElementos() 
	{
		int contador = 0;
		Node<T> act = primero;
		while(act != null)
		{
			contador ++;
			act = act.getNext();
		}
		return contador;
	}

	@Override
	public T darElementoPosicionActual() 
	{
		if(actual!=null)
		{
			return actual.getItem();
		}
		else
			return null;
	}

	@Override
	public boolean avanzarSiguientePosicion() 
	{
		if(actual != null && actual.getNext() != null)
		{
			actual = actual.getNext();
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() 
	{
		Node<T> act = primero;
		while(act != null)
		{
			if (act.getNext() != null && act.getNext().equals(actual))
			{
				actual = act;
				return true;
			}
			act = act.getNext();
		}
		return false;
	}

	@Override
	public T eliminarElemento(int pos) 
	{
		int i=0;
		T resp = null;
		Node<T> anterior = null;
		Node<T> siguiente = null;
		if (primero!= null)
		{
			resp = primero.getItem();
			actual = primero;
			while(i<pos)
			{
				actual = actual.getNext();
				if(actual!=null&&actual.getNext()!=null)
					siguiente = actual.getNext();
				i++;
			}
			resp = actual.getItem();
			if(siguiente!=null)
				siguiente.setNext(actual);
		}
		listSize--;
		return resp;
	}
	
	

	@Override
	public boolean isEmpty() 
	{
		if(listSize==0)
		{
			return true;
		}
		else
			return false;
	}
}