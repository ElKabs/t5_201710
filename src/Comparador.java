

import java.util.Comparator;

public class Comparador<T> implements Comparator
{

	@Override
	public int compare(Object o1, Object o2) 
	{
		T a = (T) o1;
		T b = (T) o2;
		String a1 = a.toString();
		String a2 = b.toString();
		return a1.compareToIgnoreCase(a2);
	}

}
