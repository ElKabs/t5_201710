public class MaxHeapCP<T extends Comparable<T>> 
{
	private T[] pq;
	private int N = 0;
	private int altura = 0;
	
	public MaxHeapCP(int maxN)
	{
		altura = maxN;
		crearCP(maxN);
	}
	
	public void crearCP(int max)
	{
		pq = (T[]) new Comparable[max+1];
	}
	
	public int darNumElementos()
	{
		return N;
	}
	
	public void agregar(T elemento)
	{
		pq[++N] = elemento;
		swim(N);
	}
	
	public T max()
	{
		T max = pq[1];
		exch(1, N--);
		pq[N+1]=null;
		sink(1);
		return max;
	}
	
	public int tamanoMax()
	{
		return altura;
	}

	private void sink(int i) 
	{
		while(2*i <= N)
		{
			int j = 2*i;
			if(j < N && less(j, j+1))
			{
				j++;
			}
			if(!less(i, j))
			{
				break;
			}
			exch(i, j);
			i = j;
		}
	}

	private void swim(int k) 
	{
		while(k > 1 && less(k/2, k))
		{
			exch(k/2, k);
			k = k/2;
		}
	}
	
	private void exch(int i, int k) 
	{
		T t = pq[i];
		pq[i] = pq[k];
		pq[k] = t;
	}

	private boolean less(int i, int k)
	{
		return pq[i].compareTo(pq[k])<0;
	}
	
	private boolean esVacia()
	{
		if(N==0)
		{
			return false;
		}
		else
			return true; 
	}
}


